<?php

namespace Drupal\Tests\node_rss\Functional;

use Drupal\Core\Url;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\RoleInterface;

/**
 * Test the functionality of the session inspector module.
 *
 * @group session_inspector
 */
class NodeRssTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node_rss',
    'node',
  ];

  /**
   * The theme to install as the default for testing.
   *
   * @var string
   */
  public $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create format will access to create all HTML tags.
    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
      'filters' => [],
    ])->save();
  }

  /**
   * Test that a user cannot access the RSS feed with no permission.
   */
  public function testRssPageIsAvailableWithNoPermissions() {
    // Create a sample node.
    $this->drupalCreateContentType(['type' => 'page']);
    $nodeOptions = [
      'title' => 'Node',
      'type' => 'page',
      'body' => [
        'value' => '<p>Content <a href="/test-node-1">This page</a>.</p>',
        'format' => 'full_html',
      ],
      'status' => 1,
    ];
    $node = $this->createNode($nodeOptions);

    // Create path for node.
    $entity_type_manager = \Drupal::entityTypeManager();
    $alias_storage = $entity_type_manager->getStorage('path_alias');
    $alias_storage->create([
      'path' => '/node/' . $node->id(),
      'alias' => '/test-node-1',
    ])->save();

    // Grab the /rss page and assert access is denied.
    $this->drupalGet('test-node-1/rss');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test that a user can access the RSS feed with the correct data.
   *
   * This includes converting relative links to absolute links.
   */
  public function testRssPageIsAvailableWithCorrectPermissions() {
    // Set up permissions.
    $permissions = [
      'access content',
      'node.view all rss feeds',
    ];
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, $permissions);

    // Create a sample node.
    $this->drupalCreateContentType(['type' => 'page']);
    $nodeOptions = [
      'title' => 'Node',
      'type' => 'page',
      'body' => [
        'value' => '<p>A link to <a href="/test-node-1">this page</a>.</p>',
        'format' => 'full_html',
      ],
      'status' => 1,
    ];
    $node = $this->createNode($nodeOptions);

    $path = '/test-node-1';

    // Create path for node.
    $entity_type_manager = \Drupal::entityTypeManager();
    $alias_storage = $entity_type_manager->getStorage('path_alias');
    $alias_storage->create([
      'path' => '/node/' . $node->id(),
      'alias' => $path,
    ])->save();

    // Grab the /rss page and assert returned content is correct.
    $this->drupalGet($path . '/rss');
    $this->assertSession()->statusCodeEquals(200);

    $url = Url::fromUri('internal:/');
    $frontPageUrl = $url->setAbsolute()->toString();

    // Due to the way in which URLs are generated on Drupal's GitLab CI we need
    // to deconstruct the body assertion into separate parts. This is because
    // the value of $frontPageUrl is "http://localhost/ whereas the web server
    // on GitLab responds to "http://localhost/web" so we can only assert that
    // the domain is present.
    $this->assertSession()->responseContains('&lt;p&gt;A link to &lt;a href="');
    $this->assertSession()->responseContains($frontPageUrl);
    $this->assertSession()->responseContains('"&gt;This page&lt;/a&gt;.&lt;/p&gt;');
  }

}
