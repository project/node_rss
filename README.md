# Node RSS

This module allows an RSS version of a node page to be displayed by appending
/rss to the end of the path. The node is displayed using the default format.

Appending /rss works for the node ID page of /node/123/rss and for the path
of the node (eg. /some/path/rss).

Only users with the permission of `node.view all rss feeds` can view the feeds.
Add this to anonymous users if required.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/node_rss).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/node_rss).

## Table of contents (optional)

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements (required)

This module requires no modules outside of Drupal core.

## Installation (required, unless a separate INSTALL.md is provided)

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration (required)

1. Enable the module at Administration > Extend.
2. Give roles the permission of `node.view all rss feeds` to view the feed.
3. This module has no configuration options.

## Maintainers (optional)

- Philip Norton - [philipnorton42](https://www.drupal.org/u/philipnorton42)
