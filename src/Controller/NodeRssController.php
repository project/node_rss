<?php

namespace Drupal\node_rss\Controller;

use Drupal\Core\Access\AccessException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for node_rss routes.
 */
class NodeRssController extends ControllerBase {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * The rendering service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $language;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new self();
    $instance->setPathAliasManager($container->get('path_alias.manager'));
    $instance->setRenderer($container->get('renderer'));
    $instance->setRequest($container->get('request_stack')->getCurrentRequest());
    $instance->setLanguage($container->get('language_manager')->getCurrentLanguage());

    return $instance;
  }

  /**
   * Set the path alias manager service.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $pathAliasManager
   *   The path alias manager service.
   *
   * @return self
   *   The current object.
   */
  public function setPathAliasManager(AliasManagerInterface $pathAliasManager): self {
    $this->pathAliasManager = $pathAliasManager;
    return $this;
  }

  /**
   * Set the renderer service.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   *
   * @return self
   *   The current object.
   */
  public function setRenderer(RendererInterface $renderer): self {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Sets the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return self
   *   The current object.
   */
  public function setRequest(Request $request): self {
    $this->request = $request;
    return $this;
  }

  /**
   * Sets the current language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language to set.
   *
   * @return self
   *   The current object.
   */
  public function setLanguage(LanguageInterface $language): self {
    $this->language = $language;
    return $this;
  }

  /**
   * Builds the response.
   */
  public function rssView(NodeInterface $node) {
    if ($node->isPublished() === FALSE && $this->currentUser()->isAnonymous() === TRUE) {
      throw new AccessException('Unable to show RSS feed of unpublished page.');
    }

    // Render the node.
    $viewBuilder = $this->entityTypeManager()->getViewBuilder('node');
    $build = $viewBuilder->view($node, 'default', $this->language->getId());
    $description = trim((string) $this->renderer->render($build));

    // Construct the URL.
    $url = new Url('entity.node.canonical', ['node' => $node->id()]);
    $url = $url->setAbsolute()->toString();

    // Render the RSS feed.
    $rssTheme = [
      '#theme' => 'node_rss',
      '#link' => Url::fromRoute('<front>')->setAbsolute()->toString(),
      '#title' => $this->t('RSS Feed'),
      '#description' => $this->t('RSS for page "@page"', ['@page' => $node->getTitle()]),
      '#langcode' => $this->language->getId(),
      '#channel_link' => $url . '/rss',
      '#node_title' => $node->getTitle(),
      '#node_url' => $url,
      '#node_guid' => $url,
      '#node_created' => date('r', $node->getCreatedTime()),
      '#node_description' => $description,
    ];
    $content = trim((string) $this->renderer->render($rssTheme));

    // Create the response and return.
    $response = new Response();
    $response->headers->set('Content-Type', 'application/rss+xml; charset=utf-8');
    $response->setContent($content);
    return $response;
  }

}
