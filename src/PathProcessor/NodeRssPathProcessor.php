<?php

namespace Drupal\node_rss\PathProcessor;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides path rewrite inbound and outbound processor.
 *
 * This class allows us to convert the path "path/rss" into "node/1/rss" and
 * show this as an external URL without altering any path aliases.
 */
class NodeRssPathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The path alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Creates a NodeRssPathProcessor object.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $pathAliasManager
   *   The alias manager interface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(AliasManagerInterface $pathAliasManager, LanguageManagerInterface $languageManager) {
    $this->pathAliasManager = $pathAliasManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    if (preg_match('/\/rss$/', $path) === 0) {
      // String is not an RSS feed string.
      return $path;
    }

    $nonRssPath = str_replace('/rss', '', $path);
    $internalPath = $this->pathAliasManager->getPathByAlias($nonRssPath, $this->languageManager->getCurrentLanguage()->getId());

    if ($internalPath === $nonRssPath && preg_match('/^node\//', $internalPath) === 0) {
      // No matching path was found, or, it wasn't a node path that we have.
      return $path;
    }

    return $internalPath . '/rss';
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL): string {
    if (preg_match('/^\/node\/.*?\/rss?$/', $path) === 0) {
      // String is not an RSS feed string.
      return $path;
    }

    $nonRssPath = str_replace('/rss', '', $path);
    $alias = $this->pathAliasManager->getAliasByPath($nonRssPath, $this->languageManager->getCurrentLanguage()->getId());

    if ($nonRssPath === $alias) {
      // An internal alias was not found.
      return $path;
    }

    return $alias . '/rss';
  }

}
